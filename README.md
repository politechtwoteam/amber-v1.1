# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version AMBER v1.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

## WAŻNE ##
* jeśli testy wam nie odpalą, albo program się nie skompiluje, to w folderze Dodatki jest Nunit, którego trzeba zainstalować
* kolejnym krokiem po instalacji jest jego dodanie do referencji, jak klikniecie w AddReference na projekcie, to wybierzcie Assemblies > Extensions i wyszukajcie NUnit.Framework i dodajcie do ref. Na nugecie jest trefna wersja.
* jeśli nadal nie macie możliwości odpalenia testów to 1. zainstalujcie R# ale nie w wersji 10.0 i wyższej, tylko niższą.

* Summary of set up
 > get clone git repository url
 > set local repodirectory
 > clone repo
 > build project (to restore NuGet packages)
 > Run it
* Configuration
* Dependencies
* Database configuration
* How to run tests

* Deployment instructions
 > make new branch on local machine
 > commit to this branch
 > create pull request
 > wait for comments and aproves
 > wait for merge or fix your bags

### Contribution guidelines ###

* Writing Code
Struktura:
 > Interfejs
 > Implementacja
 > Model abstrakcyjny
 > Model widoku
 > Wykorzystanie metod interfejsu w GUI
Przebieg procesów
 > zainicjowanie interfejsu w sekcji private dla głównego okna
   np. klasa Wypłata dziedziczy po interfejsie operacjaBankowa
   sekcja private: private OperacjaBankowa operacjaBankowa;
 > utworzenie dowązania do implementacji poprzez utworzenie nowego obiektu o typie pytanej klasy.
   sekcja inicjalizacji operacjaBankowa = new Wyplata(parametry);
 > wykorzystanie metod interfejsu zaimplementowanych w klasie Wyplata()
   sposób wykorzystania: operacjaBankowa.Metoda();

InfoDictionaty => sesja otwarta po zalogowaniu się naszego użytkownika do systemu. 
(zamykana po jego wylogowaniu/zamknięciu systemu/usunięciu)
 > sposób przekazywania
   W przypadku każdego konstruktora obiektu jaki chcemy wywołać będzie przekazana sesja wewnątrz znajdzie się zawsze metoda prywatna zapisująca każdą operację do słownika (tudzież listy), z której korzystać będzie następnie historia.

Interfejs
 > Interfejs musi pozwalać na swobodne przechodzenie między operacjami
 > Wykonanie operacji bankowej powinno być zawsze zapisywane w historii i w przypadku wywołania akcji z modułu wpłat/wypłat/przelewów informacja trafia na stos historie,
   gdzie jest przechowywana i gotowa do wyświetlenia.
* Writing tests
* Code review
**IF YOU SEE PULL REQUEST WAITING FOR APPROVE, checkout repo to pullrequest branch, check and approve, other way it will never be mearged.**
* Other guidelines

### Who do I talk to? ###

* Repo owner politechtwoteam