﻿using System;
using AMBER_v1._1.Helpers;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations.Przelew
{
    public class PrzelewStaly : IPrzelew
    {
        private IKonto konto;
        private IHistoria historia;

        private float kwota;
        
        private OffsetType typRealizacji;

        private int offset;

        private DateTime startDate;

        public PrzelewStaly(IKonto konto, IHistoria historia, float kwota, OffsetType rodzaj, int odstep, DateTime start)
        {
            this.konto = konto;
            this.historia = historia;
            this.kwota = kwota;
            this.typRealizacji = rodzaj;
            this.offset = odstep;
            this.startDate = start;
        }

        public void Wykonaj(int kontoIdFirst, int kontoIdSecond, DateTime actualDateTime)
        {
            bool warunek = false;

            if (offset > 0)
            {
                switch (typRealizacji)
                {
                    case (OffsetType.dzien):
                        warunek = ((actualDateTime - startDate).Days % offset) == 0 && actualDateTime.Hour == startDate.Hour && actualDateTime.Minute == startDate.Minute;
                        break;
                    case (OffsetType.tydzien):
                        warunek = (((actualDateTime - startDate).Days / 7) % offset) == 0 && actualDateTime.Hour == startDate.Hour && actualDateTime.Minute == startDate.Minute;
                        break;
                    case (OffsetType.miesiac):
                        warunek = (((actualDateTime.Month - startDate.Month) + 12 * (actualDateTime.Year - startDate.Year)) %
                                   offset) == 0 && actualDateTime.Day == startDate.Day && actualDateTime.Hour == startDate.Hour && actualDateTime.Minute == startDate.Minute;
                        break;
                }
            }

            if (warunek)
            {

                var k = konto.GetKontoById(kontoIdFirst);
                var kS = konto.GetKontoById(kontoIdSecond);

                konto.SetKontoSaldo(kontoIdFirst, -kwota);
                konto.SetKontoSaldo(kontoIdSecond, kwota);

                historia.SetHistoriaWpis(typeof (Przelew).ToString(), kontoIdFirst, kwota, DateTime.Now, kontoIdSecond);
                konto.UpdateKonto(k,
                    historiaWpis:
                        historia.SetHistoriaWpis(typeof (Przelew).ToString(), k.IdKonta, kwota, DateTime.Now, kS.IdKonta));
                konto.UpdateKonto(kS,
                    historiaWpis:
                        historia.SetHistoriaWpis(typeof (Przelew).ToString(), k.IdKonta, kwota, DateTime.Now, kS.IdKonta));
            }
        }

        public void Cofnij(int historiaId)
        {
            var h = historia.GetHistoriaById(historiaId);

            konto.SetKontoSaldo(h.KontoFirst.IdKonta, kwota);
            konto.SetKontoSaldo(h.KontoSecond.IdKonta, -kwota);

            konto.UpdateKonto(h.KontoSecond, historiaWpis: historia.SetHistoriaWpis(typeof(Przelew).ToString(), h.KontoSecond.IdKonta, kwota, DateTime.Now, h.KontoFirst.IdKonta));
            konto.UpdateKonto(h.KontoFirst, historiaWpis: historia.SetHistoriaWpis(typeof(Przelew).ToString(), h.KontoSecond.IdKonta, kwota, DateTime.Now, h.KontoFirst.IdKonta));
        }
    }
}
