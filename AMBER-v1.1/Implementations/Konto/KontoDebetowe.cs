﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations
{
    public class KontoDebetowe : Konto
    {
        private float Debet { get; set; }
        private IKonto iKonto;

        public KontoDebetowe(IKonto iKonto)
        {
            this.iKonto = iKonto;
        }

        public KontoDebetowe(float debet, IKonto iKonto)
        {
            this.Debet = debet;
            this.iKonto = iKonto;
        }

        public float GetKontoSaldo(int idKonta)
        {
            var konto = iKonto.GetKontoById(idKonta);
            return Debet != null ? Debet + konto.Saldo : konto.Saldo;
        }

        public void SetDebet(int idKonta)
        {
            var konto = iKonto.GetKontoById(idKonta);
            konto.Debet = this.Debet;
        }
    }
}
