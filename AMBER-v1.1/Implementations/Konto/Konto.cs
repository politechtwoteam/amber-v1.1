﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations
{
    public class Konto : IKonto
    {
        private IHistoria historia;
        private IKredyt kredyt;
        private IOdsetkiKonto odsetki;

        private List<KontoModel> ListaKont; 

        public Konto(IHistoria historia, IKredyt kredyt)
        {
            this.historia = historia;
            this.kredyt = kredyt;
        }

        public Konto(IOdsetkiKonto odsetki)
        {
            this.odsetki = odsetki;
        }

        public Konto()
        {
            
        }

        public Konto (string nazwa, string haslo)
        {
            var konto  = new KontoModel();
            konto.IdKonta = GetLastKontoId() + 1;
            konto.NazwaKonta = nazwa;
            konto.Haslo = haslo;
            konto.DataUtworzenia = DateTime.Now;
        }

        public  KontoModel GetKontoById(int id)
        {
            return GetAllKontaList().Where(konto => konto.IdKonta == id).FirstOrDefault();
        }

        public KontoModel CreateKonto(string nazwa, string haslo)
        {
            var konto = new KontoModel();
            konto.IdKonta = GetLastKontoId() + 1;
            konto.NazwaKonta = nazwa;
            konto.Haslo = haslo;
            konto.DataUtworzenia = DateTime.Now;
 	        return konto;
        }

        private int GetLastKontoId()
        {
            return 1;
        }

        public void SetHaslo(int kontoId, string starehaslo, string haslo)
        {
            var konto = GetKontoById(kontoId) ;
 	        if(starehaslo == konto.Haslo)
            {
                konto.Haslo = haslo;
            }
        }

        public void SetKontoSaldo(int kontoId, float kwota)
        {
            var konto = GetKontoById(kontoId);
            konto.Saldo += kwota;
        }

        public bool AutoryzujKonto(int kontoId, string login, string haslo)
        {
            var konto = GetKontoById(kontoId);
 	        if(login == konto.NazwaKonta && haslo == konto.Haslo)
            {
                return true;
            }

            return false;
        }
    
        public  List<KontoModel> GetAllKontaList()
        {
            var listaKont = ListaKont;
            return listaKont;
        }


        public void UpdateKonto(KontoModel konto, KredytModel kredytNowy = null, HistoriaModel historiaWpis = null)
        {
            if (kredytNowy != null)
                konto.KredytList.Add(kredytNowy);
            if (historiaWpis != null)
                konto.HistoriaList.Add(historiaWpis);
        }


        public void TestowaListaKont(List<KontoModel> lista)
        {
            ListaKont = lista;
        }


        public void LiczOdsetki(int idKonta)
        {
            var konto = this.GetKontoById(idKonta);

            this.SetKontoSaldo(idKonta, odsetki.CreateOdsetki(konto.Saldo));
        }
    }

    public class KontoModel
    {
        public KontoModel()
        {
            HistoriaList = new List<HistoriaModel>();
            KredytList = new List<KredytModel>();
        }
        
        public int IdKonta { get; set; }
        public string NazwaKonta { get; set; }
        public string Haslo { get; set; }
        public float Saldo { get; set; }
        public float Debet { get; set; }
        public DateTime DataUtworzenia { get; set; }
        public List<HistoriaModel> HistoriaList { get; set; }
        public List<KredytModel> KredytList { get; set; }
    }
}
