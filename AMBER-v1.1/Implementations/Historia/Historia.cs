﻿using System;
using System.Collections.Generic;
using System.Linq;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations
{
    public class Historia : IHistoria
    {
        private IKonto konto;
 
        public Historia(IKonto konto)
        {
            this.konto = konto;
        }

        public HistoriaModel SetHistoriaWpis(string typOperacji, int kontoId, float kwota, DateTime dataOperacji, int? kontoIdSecond)
        {
            var historiaWpis = new HistoriaModel();
            historiaWpis.HistoriaId = GetAllHistoriaList().Count;
            historiaWpis.KontoFirst = konto.GetKontoById(kontoId);
            historiaWpis.KontoSecond = kontoIdSecond.HasValue ? konto.GetKontoById(kontoIdSecond.Value) : null;
            historiaWpis.DataOperaji = dataOperacji;
            historiaWpis.TypOperacji = typOperacji;
            historiaWpis.Kwota = kwota;
            return historiaWpis;
        }

        public List<HistoriaModel> GetAllHistoriaForKonto(int kontoId)
        {
            var k = konto.GetKontoById(kontoId);
            return k.HistoriaList.Where(historia => historia.KontoFirst.IdKonta == kontoId).ToList();
        }

        public List<HistoriaModel> GetAllHistoriaList()
        {
            var listaHistorii = new List<HistoriaModel>();
            return listaHistorii;
        }

        private void AddToHistoriaList(HistoriaModel historiaWpis)
        {
            GetAllHistoriaList().Add(historiaWpis);
        }


        public HistoriaModel GetHistoriaById(int historiaId)
        {
            return GetAllHistoriaList().Where(h => h.HistoriaId == historiaId).FirstOrDefault();
        }
    }

    public class HistoriaModel
    {
        public int HistoriaId { get; set; }
        public KontoModel KontoFirst { get; set; }
        public KontoModel KontoSecond { get; set; }
        public DateTime DataOperaji { get; set; }
        public float Kwota { get; set; }
        public string Komunikat { get; set; }
        public string TypOperacji { get; set; }
    }
}
