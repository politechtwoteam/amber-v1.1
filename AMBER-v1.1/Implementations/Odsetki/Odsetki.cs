﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations
{
    public class Odsetki : IOdsetki
    {
        private IKredyt kredyt;
        public Odsetki(IKredyt kredyt)        
        {
            this.kredyt = kredyt;
        }
        
        public void CreateOdsetki(int kredytId)
        {
            var noweRaty = new Dictionary<DateTime, float>();
            var r = kredyt.GetKredytById(kredytId);
            var stareRaty = r.Raty;

            float i = 0;
            foreach (var m in stareRaty)
            {
                noweRaty.Add(m.Key, m.Value * ( i++ / 100) + m.Value );
            }

            kredyt.SetOprocentowanieForKredyt(r.KredytId, noweRaty, noweRaty.Sum(x => x.Value));
        }
    }
}
