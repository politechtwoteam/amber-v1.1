﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations
{
    class KalkulatorOdsetekKwartalnych : IOdsetkiKonto
    {
        public float CreateOdsetki(float saldo)
        {
            var odsetki = 0f; 
            if (saldo < 1000)
                odsetki = saldo * 0.05f;
            else if(saldo < 5000)
                odsetki = saldo * 0.06f;
            else if (saldo < 10000)
                odsetki = saldo * 0.07f;
            else if (saldo < 15000)
                odsetki = saldo * 0.08f;
            else if (saldo < 25000)
                odsetki = saldo * 0.1f;

            return odsetki;
        }
    }
}
