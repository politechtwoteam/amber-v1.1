﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations
{
    class KalkulatorOdsetekZnajomySzefaKwartalnych : IOdsetkiKonto
    {
        public float CreateOdsetki(float saldo)
        {
            var odsetki = 0f;
            if (saldo < 500)
                odsetki = saldo * 0.1f;
            else if (saldo < 1000)
                odsetki = saldo * 0.2f;
            else if (saldo < 2000)
                odsetki = saldo * 0.3f;
            else if (saldo < 5000)
                odsetki = saldo * 0.4f;
            else if (saldo < 8000)
                odsetki = saldo * 0.9f;

            return odsetki;
        }
    }
}
