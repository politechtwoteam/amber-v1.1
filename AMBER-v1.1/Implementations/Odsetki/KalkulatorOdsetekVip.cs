﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations
{
    class KalkulatorOdsetekVipKwartalnych : IOdsetkiKonto
    {
        public float CreateOdsetki(float saldo)
        {
            var odsetka = 0f;
            if (saldo < 500)
                odsetka = saldo * 0.05f;
            else if (saldo < 1000)
                odsetka = saldo * 0.06f;
            else if (saldo < 2000)
                odsetka = saldo * 0.07f;
            else if (saldo < 5000)
                odsetka = saldo * 0.08f;
            else if (saldo < 8000)
                odsetka = saldo * 0.1f;

            return odsetka;
        }
    }
}
