﻿using System;
using System.Collections.Generic;
using System.Linq;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations.OperacjaBankowa
{
    public class Wplata : IOperacjaBankowa
    {
        private IKonto konto;
        private IHistoria historia;

        private float kwota;
        private int kontoId;

        public Wplata(float kwota, int kontoId, IHistoria historia,IKonto konto)
        {
            this.konto = konto;
            this.historia = historia;

            this.kwota = Math.Abs(kwota);
            this.kontoId = kontoId;
            Wykonaj();
        }

        public void Wykonaj()
        {
            var k = konto.GetKontoById(kontoId);

            konto.SetKontoSaldo(kontoId, kwota);

            historia.SetHistoriaWpis(typeof(Wplata).ToString(), k.IdKonta, kwota, DateTime.Now, null);
        }


        public void Cofnij(int historiaId)
        {
            var h = historia.GetHistoriaById(historiaId);

            konto.SetKontoSaldo(h.KontoFirst.IdKonta, -kwota);
            
            konto.UpdateKonto(h.KontoFirst, historiaWpis: historia.SetHistoriaWpis(typeof(Wplata).ToString(), h.KontoFirst.IdKonta, -kwota, DateTime.Now,null));
        }
    }
}
