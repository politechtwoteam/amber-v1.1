﻿using System;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations.OperacjaBankowa
{
    class Wyplata : IOperacjaBankowa
    {
        private IKonto konto;
        private IHistoria historia;

        private float kwota;
        private int kontoId;

        public Wyplata(float kwota, int kontoId, IHistoria historia, IKonto konto)
        {
            this.konto = konto;
            this.historia = historia;

            this.kwota = Math.Abs(kwota);
            this.kontoId = kontoId;
            Wykonaj();
        }

        public void Wykonaj()
        {
            var k = konto.GetKontoById(kontoId);

            if (k.Saldo > kwota)
            {
              konto.SetKontoSaldo(k.IdKonta,-1 * kwota);
              historia.SetHistoriaWpis(typeof(Wyplata).ToString(), k.IdKonta, kwota, DateTime.Now, null);
            }
            else
            {
                historia.SetHistoriaWpis("Brak środków", k.IdKonta, kwota, DateTime.Now, null);
            }
        }



        public void Cofnij(int historiaId)
        {
            var h = historia.GetHistoriaById(historiaId);

            konto.SetKontoSaldo(h.KontoFirst.IdKonta, kwota);

            konto.UpdateKonto(h.KontoFirst, historiaWpis: historia.SetHistoriaWpis(typeof(Wyplata).ToString(), h.KontoFirst.IdKonta, kwota, DateTime.Now, null));
        }
    }
}
