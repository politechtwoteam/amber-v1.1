﻿using System;
using System.Linq;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations.Przelew
{
    class Przelew : IPrzelew
    {
        private IKonto konto;
        private IHistoria historia;

        private float kwota;

        public Przelew(IKonto konto, IHistoria historia, float kwota)
        {
            this.konto = konto;
            this.historia = historia;

            this.kwota = kwota;
        }

        public void Wykonaj(int kontoIdFirst, int kontoIdSecond, DateTime actualDateTime)
        {
            var k = konto.GetKontoById(kontoIdFirst);
            var kS = konto.GetKontoById(kontoIdSecond);

            konto.SetKontoSaldo(kontoIdFirst, -kwota);
            konto.SetKontoSaldo(kontoIdSecond, kwota);

            historia.SetHistoriaWpis(typeof(Przelew).ToString(), kontoIdFirst, kwota, DateTime.Now, kontoIdSecond);
            konto.UpdateKonto(k, historiaWpis: historia.SetHistoriaWpis(typeof(Przelew).ToString(), k.IdKonta, kwota, DateTime.Now, kS.IdKonta));
            konto.UpdateKonto(kS, historiaWpis: historia.SetHistoriaWpis(typeof(Przelew).ToString(), k.IdKonta, kwota, DateTime.Now, kS.IdKonta));
        }

        public void Cofnij(int historiaId)
        {
            var h = historia.GetHistoriaById(historiaId);

            konto.SetKontoSaldo(h.KontoFirst.IdKonta, kwota);
            konto.SetKontoSaldo(h.KontoSecond.IdKonta, -kwota);

            konto.UpdateKonto(h.KontoSecond, historiaWpis: historia.SetHistoriaWpis(typeof(Przelew).ToString(), h.KontoSecond.IdKonta, kwota, DateTime.Now, h.KontoFirst.IdKonta));
            konto.UpdateKonto(h.KontoFirst, historiaWpis: historia.SetHistoriaWpis(typeof(Przelew).ToString(), h.KontoSecond.IdKonta, kwota, DateTime.Now, h.KontoFirst.IdKonta));
        }
    }
}
