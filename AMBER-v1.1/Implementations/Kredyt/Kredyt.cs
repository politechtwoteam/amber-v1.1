﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1.Implementations
{
    public class Kredyt : IKredyt
    {
        private IKonto konto;
        private IOdsetki odsetki;

        private List<KredytModel> ListaKredytow; 

        public Kredyt(IKonto konto)
        {
            this.konto = konto;
            this.odsetki = odsetki;
        }
        
        public KredytModel CreateKredyt(int kontoId, float kwotaKretytu, int iloscRat)
        {
            var kredyt = new KredytModel();
            var kwotaraty = kwotaKretytu / iloscRat;
            kredyt.Konto = konto.GetKontoById(kontoId);
            kredyt.KwotaKredytu = kwotaKretytu;
            kredyt.KredytId = GetAllKredytList().Count + 1;

            for (int i = 1; i <= iloscRat; i++)
            {
                kredyt.Raty.Add(DateTime.Now.AddMonths(i), kwotaraty);
            }
            
            return kredyt;
        }

        public KredytModel GetKredytById(int id)
        {
            return GetAllKredytList().Where(kredyt => kredyt.KredytId == id).FirstOrDefault();
        }


        private int GetLastKredytId()
        {
            return GetAllKredytList().OrderBy(x => x.KredytId).Select(x => x.KredytId).Last();
        }

        public List<KredytModel> GetAllKredytList()
        {
            var listaKredytów = ListaKredytow;
            return listaKredytów;
        }

        public List<KredytModel> GetAllKredytListForKonto(int kontoId)
        {
            return GetAllKredytList().Where(kredyt => kredyt.Konto.IdKonta == kontoId).ToList();
        }


        public void SetOprocentowanieForKredyt(int kredytId, Dictionary<DateTime, float> raty, float kwotaKredytu)
        {
            var kredyt = GetKredytById(kredytId);
            kredyt.Raty = raty;
            kredyt.KwotaKredytu = kwotaKredytu;
        }


        public void TestowaLista(List<KredytModel> lista)
        {
            ListaKredytow = lista;
        }
    }

    public class KredytModel
    {
        public KredytModel()
        {
            Raty = new Dictionary<DateTime, float>();
        }

        public int KredytId { get; set; }
        public Dictionary<DateTime, float> Raty { get; set; }
        public float KwotaKredytu { get; set; }
        public KontoModel Konto { get; set; }
    }
}
