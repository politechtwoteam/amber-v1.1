﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Implementations;

namespace AMBER_v1._1.Interfaces
{
    public interface IHistoria
    {
        HistoriaModel SetHistoriaWpis(string typOperacji, int kontoId, float kwota, DateTime dataOperacji, int? kontoIdSecond);
        
        HistoriaModel GetHistoriaById(int historiaId);

        List<HistoriaModel> GetAllHistoriaForKonto(int kontoId);

        List<HistoriaModel> GetAllHistoriaList();
    }
}
