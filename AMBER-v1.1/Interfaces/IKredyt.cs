﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Implementations;

namespace AMBER_v1._1.Interfaces
{
    public interface IKredyt
    {
        KredytModel CreateKredyt(int kontoId, float kwotaKretytu, int iloscRat);
        KredytModel GetKredytById(int id);
        List<KredytModel> GetAllKredytList();
        List<KredytModel> GetAllKredytListForKonto(int kontoId);
        void SetOprocentowanieForKredyt(int kredytId, Dictionary<DateTime, float> raty, float kwotaKredytu);
        void TestowaLista(List<KredytModel> lista);
    }
}
