﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;

namespace AMBER_v1._1.Interfaces
{
    public interface IOperacjaBankowa
    {
        void Wykonaj();

        void Cofnij(int historiaId);
    }
}
