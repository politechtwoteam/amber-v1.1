﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Implementations;

namespace AMBER_v1._1.Interfaces
{
    public interface IKonto
    {
        KontoModel GetKontoById(int id);
        KontoModel CreateKonto(string nazwa, string haslo);
        void SetHaslo(int kontoId, string starehaslo, string haslo);
        void SetKontoSaldo(int kontoId, float kwota);
        bool AutoryzujKonto(int kontoId, string login, string haslo);
        List<KontoModel> GetAllKontaList();
        void UpdateKonto(KontoModel konto, KredytModel kredytNowy = null, HistoriaModel historiaWpis = null);
        void LiczOdsetki(int idKonta);
        void TestowaListaKont(List<KontoModel> lista);
    }
}
