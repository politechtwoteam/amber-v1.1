﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMBER_v1._1.Interfaces
{
    public interface IOdsetkiKonto
    {
        float CreateOdsetki(float saldo);
    }
}
