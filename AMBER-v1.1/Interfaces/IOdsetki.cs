﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Implementations;

namespace AMBER_v1._1.Interfaces
{
    public interface IOdsetki
    {
        void CreateOdsetki(int kredytId);
    }
}
