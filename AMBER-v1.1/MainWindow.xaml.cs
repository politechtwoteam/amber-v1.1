﻿using System.Collections.Generic;
using System.Windows;
using AMBER_v1._1.Interfaces;

namespace AMBER_v1._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }
}
