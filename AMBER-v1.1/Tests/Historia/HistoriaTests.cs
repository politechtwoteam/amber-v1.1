﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using AMBER_v1._1.Interfaces;
using AMBER_v1._1.Implementations;
using Moq;
using NUnit.Framework;

namespace AMBER_v1._1.Tests.Historia
{
    public class HistoriaTests : TestModels
    {
        private IHistoria historia;
        private Mock<IKonto> kontoMock;
        private IKonto konto;

        [SetUp]
        public void SetUp()
        {
            kontoMock = new Mock<IKonto>();
            historia = new Implementations.Historia(kontoMock.Object);
        }

        [Test]
        public void AddHistoriaWplataToKonto()
        {
            kontoMock.Setup(x => x.GetKontoById(1)).Returns(KontoTestModel);
            var k = kontoMock.Object.GetKontoById(1);
            var h = historia.SetHistoriaWpis("Wplata", 1, 200, DataOperacji, null);

            Assert.AreEqual(k, h.KontoFirst);
        }

        [Test]
        public void HistoriaPrzelewu()
        {
            kontoMock.Setup(x => x.GetKontoById(1)).Returns(KontoTestModel);
            kontoMock.Setup(x => x.GetKontoById(2)).Returns(KontoTestModel2);
            var k = kontoMock.Object.GetKontoById(1);
            var k2 = kontoMock.Object.GetKontoById(2);
            var h = historia.SetHistoriaWpis("Przelew", 1, 200, DataOperacji, 2);

            Assert.AreEqual(k2, h.KontoSecond);
        }

        [Test]
        public void PobranieHistoriiPoId()
        {
            KontoTestModel.HistoriaList = HistoriaTestList();
            kontoMock.Setup(x => x.GetKontoById(1)).Returns(KontoTestModel);
            var h = historia.GetAllHistoriaForKonto(1);

            Assert.AreEqual(2, h.Count);
        }
    }

    public class TestModels
    {
        public DateTime DataOperacji =  new DateTime(2015,11,20);
        public KontoModel KontoTestModel = new KontoModel
        {
            IdKonta = 1,
            Saldo = 2140.00f,
            HistoriaList = new List<HistoriaModel>(),
            KredytList = null,
            DataUtworzenia = new DateTime(2015,11,20),
            Haslo = "a",
            NazwaKonta = "JanuszKowalski"
        };

        public KontoModel KontoTestModel2 = new KontoModel
        {
            IdKonta = 2,
            Saldo = 2540.00f,
            HistoriaList = new List<HistoriaModel>(),
            KredytList = null,
            DataUtworzenia = new DateTime(2015, 11, 20),
            Haslo = "b",
            NazwaKonta = "JanuszRudnik"
        };

        public HistoriaModel HistoriaTestModel = new HistoriaModel
        {
            HistoriaId = 1,
            KontoSecond = null,
            KontoFirst = null,
            TypOperacji = "",
            DataOperaji = new DateTime(2015,11,20),
            Komunikat = ""
        };

        public List<HistoriaModel> HistoriaTestList()
        {
            var list = new List<HistoriaModel>();
            list.Add(new HistoriaModel
            {
                HistoriaId = 1,
                KontoSecond = null,
                KontoFirst = KontoTestModel,
                TypOperacji = "",
                DataOperaji = new DateTime(2015, 11, 20),
                Komunikat = ""
            });
            list.Add(new HistoriaModel
            {
                HistoriaId = 2,
                KontoSecond = null,
                KontoFirst = KontoTestModel,
                TypOperacji = "",
                DataOperaji = new DateTime(2015, 11, 20),
                Komunikat = ""
            });
            list.Add(new HistoriaModel
            {
                HistoriaId = 3,
                KontoSecond = null,
                KontoFirst = KontoTestModel2,
                TypOperacji = "",
                DataOperaji = new DateTime(2015, 11, 20),
                Komunikat = ""
            });

            return list;
        }
    }
}
