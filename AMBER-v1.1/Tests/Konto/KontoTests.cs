﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Implementations;
using AMBER_v1._1.Interfaces;
using Moq;
using NUnit.Framework;

namespace AMBER_v1._1.Tests.Konto
{
    class KontoTests : TestModels
    {
        private IKonto iKonto;
        private Mock<IHistoria> historiaMock;
        private Mock<IKredyt> kredytMock;
        private static KontoModel Konto1;
        private static KontoModel Konto2;

        [SetUp]
        public void SetUp()
        {
            Konto1 = KontoTestModel();
            Konto2 = KontoTestModel2();
            historiaMock = new Mock<IHistoria>();
            kredytMock = new Mock<IKredyt>();
            iKonto = new Implementations.Konto(historiaMock.Object, kredytMock.Object);
            iKonto.TestowaListaKont(KontaTestList());
            
        }

        [Test]
        public void TworzenieKonta()
        {
            var konto = iKonto.CreateKonto("nowe konto", "a");

            Assert.AreEqual("nowe konto", konto.NazwaKonta);
        }

        [Test]
        public void AutoryzacjaKonta()
        {
            Assert.AreEqual(true, iKonto.AutoryzujKonto(1, "JanuszKowalski","a"));
        }

        [Test]
        public void ZmianaHasla()
        {
            var noweHaslo = "nowe";
            iKonto.SetHaslo(1, "a", noweHaslo);
            var haslo = iKonto.GetKontoById(1).Haslo; 
            Assert.AreEqual(noweHaslo, haslo);
        }

        [TestCaseSource("TypyOdsetek")]
        public void CreateOdsetkiForKonto(IOdsetkiKonto iOdsetkiKonta, float expected)
        {
            iKonto = new Implementations.Konto(iOdsetkiKonta);
            iKonto.TestowaListaKont(KontaTestList());
            iKonto.LiczOdsetki(1);
            var saldo = iKonto.GetKontoById(1).Saldo;

            Assert.AreEqual(expected, saldo);
        }

        [TestCase(2240f, 100f)]
        [TestCase(2040f, -100f)]
        [TestCase(2580f, 440f)]
        public void UstawianieSalda(float expected, float addValue)
        {
            iKonto.SetKontoSaldo(1,addValue);
            var result = iKonto.GetKontoById(1).Saldo;
            Assert.AreEqual(expected, result);
        }

        [TestCaseSource("TestCaseSourceKonta")]
        public void PobierzKontoPoId(KontoModel expected, int id)
        {
            var result = iKonto.GetKontoById(id);
            Assert.AreEqual(expected.IdKonta,result.IdKonta);
        }


        public object[] TypyOdsetek = new[]
        {
            new object[] { new KalkulatorOdsetekKwartalnych(), 2268.3999f },
            new object[] { new KalkulatorOdsetekVipKwartalnych(), 2311.19995f },
            new object[] { new KalkulatorOdsetekZnajomySzefaKwartalnych(), 2996.0f }
        };

        public object[] TestCaseSourceKonta = new[]
        {
            new object[] {KontoTestModel() , 1},
            new object[] {KontoTestModel2(), 2}
        };
    }

    public class TestModels
    {
        public DateTime DataOperacji = new DateTime(2015, 11, 20);
        public static KontoModel KontoTestModel()
        {
            return new KontoModel()
            {
                IdKonta = 1,
                Saldo = 2140.00f,
                HistoriaList = new List<HistoriaModel>(),
                KredytList = new List<KredytModel>(),
                DataUtworzenia = new DateTime(2015, 11, 20),
                Haslo = "a",
                NazwaKonta = "JanuszKowalski"
            };
        }

        public static KontoModel KontoTestModel2()
        {
            return new KontoModel()
            {
                IdKonta = 2,
                Saldo = 2540.00f,
                HistoriaList = new List<HistoriaModel>(),
                KredytList = null,
                DataUtworzenia = new DateTime(2015, 11, 20),
                Haslo = "b",
                NazwaKonta = "JanuszRudnik"
            };
        }

        public List<KontoModel> KontaTestList()
        {
            var list = new List<KontoModel>();
            list.Add(KontoTestModel());
            list.Add(KontoTestModel2());
            return list;
        }

        public List<HistoriaModel> HistoriaTestList()
        {
            var list = new List<HistoriaModel>();
            list.Add(new HistoriaModel
            {
                HistoriaId = 1,
                KontoSecond = null,
                KontoFirst = KontoTestModel(),
                TypOperacji = "",
                DataOperaji = new DateTime(2015, 11, 20),
                Komunikat = ""
            });
            list.Add(new HistoriaModel
            {
                HistoriaId = 2,
                KontoSecond = null,
                KontoFirst = KontoTestModel(),
                TypOperacji = "",
                DataOperaji = new DateTime(2015, 11, 20),
                Komunikat = ""
            });
            list.Add(new HistoriaModel
            {
                HistoriaId = 3,
                KontoSecond = null,
                KontoFirst = KontoTestModel2(),
                TypOperacji = "",
                DataOperaji = new DateTime(2015, 11, 20),
                Komunikat = ""
            });

            return list;
        }
    }
}
