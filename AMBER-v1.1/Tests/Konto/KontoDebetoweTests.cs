﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Implementations;
using AMBER_v1._1.Interfaces;
using Moq;
using NUnit.Framework;

namespace AMBER_v1._1.Tests.Konto
{
    class KontoDebetoweTests : TestModels
    {
        private IKonto iKonto;
        private Mock<IHistoria> historiaMock;
        private Mock<IKredyt> kredytMock;
        private static KontoModel Konto1;
        private static KontoModel Konto2;

        [SetUp]
        public void SetUp()
        {
            Konto1 = KontoTestModel();
            Konto2 = KontoTestModel2();
            historiaMock = new Mock<IHistoria>();
            kredytMock = new Mock<IKredyt>();
            iKonto = new Implementations.Konto(historiaMock.Object, kredytMock.Object);
            iKonto.TestowaListaKont(KontaTestList());

        }


        [Test]
        public void TestDebetu()
        {
            var debetowe = new Implementations.KontoDebetowe(1200f, iKonto);
            debetowe.SetDebet(1);
            Assert.AreEqual(2140f + 1200f, debetowe.GetKontoSaldo(1));
        }

        [Test]
        public void AktualizacjaListKontaKredyt()
        {
            var debetowe = new Implementations.KontoDebetowe(iKonto);
            debetowe.SetDebet(1);
            Assert.AreEqual(2140f, debetowe.GetKontoSaldo(1));
            /////////////WZORZEC COMAND NA PO ŚWIĘTACH!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            /// // WZORZEC MEDIATOR - ///
            /// UNIT TESTY - UDNAY I NIE UDANY PRZELEW BANKOWY//
        }
    }
}
