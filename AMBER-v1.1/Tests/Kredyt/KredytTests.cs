﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Implementations;
using AMBER_v1._1.Interfaces;
using Moq;
using NUnit.Framework;

namespace AMBER_v1._1.Tests.Kredyt
{
    public class KredytTests : TestsModel
    {
        private IKredyt iKredyt;
        private IOdsetki iOdsetki;
        private Mock<IKonto> kontoMock;
        private static KredytModel kredytModel2;

        [SetUp]
        public void SetUp()
        {
            kredytModel2 = KredytModel2();
            kredytModel2.Raty.Add(DataOperacjiKredytowej, 1000);
            kredytModel2.Raty.Add(DataOperacjiKredytowej.AddMonths(1), 1000);
            var list = new List<KredytModel>();

            list.Add(KredytModel1());
            list.Add(kredytModel2);

            kontoMock = new Mock<IKonto>();
            iKredyt = new Implementations.Kredyt(kontoMock.Object);
            iKredyt.TestowaLista(list);
            iOdsetki = new Odsetki(iKredyt);
        }

        [Test]
        public void UtworzenieKredytu()
        {
            kontoMock.Setup(k => k.GetKontoById(1)).Returns(KontoTestModel());
            var kredyt = iKredyt.CreateKredyt(1, 2600f, 10);
            bool isTrue;
            isTrue = kredyt.KredytId == 3 && kredyt.KwotaKredytu == 2600f && kredyt.Raty.First().Value == 260 && kredyt.Konto.NazwaKonta == KontoTestModel().NazwaKonta ? true : false;
            
            Assert.AreEqual(true, isTrue);
        }

        [Test]
        public void UtworzenieKredytu_NaliczenieOdsetek()
        {
            iOdsetki.CreateOdsetki(2);
            bool isTrue;
            var ostatniaRata = kredytModel2.Raty.Last().Value;
            isTrue = kredytModel2.KwotaKredytu == 2010f && ostatniaRata == 1010f ? true : false;

            Assert.AreEqual(true, isTrue);
        }
    }

    public class TestsModel
    {
        public DateTime DataOperacjiKredytowej = new DateTime(2015, 11, 20);


        public KredytModel KredytModel1()
        {
            return new KredytModel()
            {
                KredytId = 1,
                Konto = KontoTestModel(),
                KwotaKredytu = 2000f,
                Raty = new Dictionary<DateTime, float>()
            };
        }

        public KredytModel KredytModel2()
        {
            return new KredytModel()
            {
                KredytId = 2,
                Konto = KontoTestModel2(),
                KwotaKredytu = 2000f,
                Raty = new Dictionary<DateTime, float>()
            };
        }

        public DateTime DataOperacji = new DateTime(2015, 11, 20);
        public static KontoModel KontoTestModel()
        {
            return new KontoModel()
            {
                IdKonta = 1,
                Saldo = 2140.00f,
                HistoriaList = new List<HistoriaModel>(),
                KredytList = new List<KredytModel>(),
                DataUtworzenia = new DateTime(2015, 11, 20),
                Haslo = "a",
                NazwaKonta = "JanuszKowalski"
            };
        }

        public static KontoModel KontoTestModel2()
        {
            return new KontoModel()
            {
                IdKonta = 2,
                Saldo = 2540.00f,
                HistoriaList = new List<HistoriaModel>(),
                KredytList = null,
                DataUtworzenia = new DateTime(2015, 11, 20),
                Haslo = "b",
                NazwaKonta = "JanuszRudnik"
            };
        }
    }
}
