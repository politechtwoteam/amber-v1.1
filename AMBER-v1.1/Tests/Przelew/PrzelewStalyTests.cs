﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Helpers;
using AMBER_v1._1.Implementations;
using AMBER_v1._1.Interfaces;
using Moq;
using NUnit.Framework;

namespace AMBER_v1._1.Tests.PrzelewStaly
{
    internal class PrzelewTests : TestModels
    {
        private Mock<IHistoria> historiaMock;
        private IKonto iKonto;
        private Mock<IKredyt> iKredyt;
        
        [SetUp]
        public void SetUp()
        {
            var listaKont = new List<KontoModel>();
            listaKont.Add(KontoTestModelforPrzelew());
            listaKont.Add(KontoTestModel2forPrzelew());


            historiaMock = new Mock<IHistoria>();

            historiaMock.Setup(h => h.SetHistoriaWpis(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<float>()
                , It.IsAny<DateTime>(), It.IsAny<int?>()));

            iKredyt = new Mock<IKredyt>();

            iKonto = new Implementations.Konto(historiaMock.Object, iKredyt.Object);
            iKonto.TestowaListaKont(listaKont);
        }

        [TestCase(2, OffsetType.dzien, 1100f, 100f, 50f,1,22)]    //przy testowaniu trzeba wstawić datę, godz. + min., w której wykonujemy test
                                                            //zakładałbym, że docelowa aplikacja będzie odpalać z automatu co minutę
        [TestCase(2, OffsetType.miesiac, 1055f, 100f, 100f, 1, 22)]
        [TestCase(2, OffsetType.tydzien, 100f, 0f, 50f, 1, 22)]
        public void PrzelewStalyTest(int offset, OffsetType typ, float expectedFirst, float expectedSecond, float value, int mc, int dzien)
        {
            DateTime start = new DateTime(2016,mc,dzien,21,0,0);
            iKonto.GetKontoById(1).Saldo = expectedFirst + value;
            iKonto.GetKontoById(2).Saldo = expectedSecond - value;
            IPrzelew przelew = new Implementations.Przelew.PrzelewStaly(iKonto, historiaMock.Object, value, typ, offset,start);
            DateTime expecteDateTime = CreateOffsetDate(offset, typ, start);

            przelew.Wykonaj(1, 2, expecteDateTime); 
            
            Assert.AreEqual(expectedFirst, iKonto.GetKontoById(1).Saldo);
            Assert.AreEqual(expectedSecond, iKonto.GetKontoById(2).Saldo);
        }

        private DateTime CreateOffsetDate(int offset, OffsetType typ, DateTime start)
        {
            if (typ == OffsetType.dzien)
                start = start.AddDays(offset);
            if (typ == OffsetType.miesiac)
                start = start.AddMonths(offset);
            if (typ == OffsetType.tydzien)
                start = start.AddDays(offset * 7);
            return start;
        }

        [TestCase(1100f, 21050f, 50f)]
        public void CofnijPrzelewStalyTest(float expectedFirst, float expectedSecond, float value)
        {
            iKonto.GetKontoById(1).Saldo = expectedFirst - value;
            iKonto.GetKontoById(2).Saldo = expectedSecond + value;
            var historia = HistoriaModel;
            historia.KontoFirst = iKonto.GetKontoById(1);
            historia.KontoSecond = iKonto.GetKontoById(2);

            historiaMock.Setup(h => h.GetHistoriaById(1)).Returns(historia);

            IPrzelew przelew = new Implementations.Przelew.Przelew(iKonto, historiaMock.Object, value);
            przelew.Cofnij(1);

            Assert.AreEqual(expectedFirst, iKonto.GetKontoById(1).Saldo);
        }
    }

    public class TestModels
        {
            public static HistoriaModel HistoriaModel = new HistoriaModel
            {
                HistoriaId = 1,
                KontoSecond = null,
                KontoFirst = null,
                TypOperacji = "",
                DataOperaji = new DateTime(2015, 11, 20),
                Komunikat = "",
                Kwota = 0
            };

            public static KontoModel KontoTestModelforPrzelew()
            {
                return new KontoModel()
                {
           
                    IdKonta = 1,
                    Saldo = 150.00f,
                    HistoriaList = new List<HistoriaModel>(),
                    KredytList = new List<KredytModel>(),
                    //typRealizacji=typ.dzien,
                    //offset=0,
                    DataUtworzenia = new DateTime(2015, 11, 20),
                    Haslo = "a",
                    NazwaKonta = "JanuszKowalski"
                };
            }

            public static KontoModel KontoTestModel2forPrzelew()
            {
                return new KontoModel()
                {
                    IdKonta = 2,
                    Saldo = 50.00f,
                    HistoriaList = new List<HistoriaModel>(),
                    KredytList = null,
                    //typRealizacji = typ.miesiac,
                    //offset = 0,
                    DataUtworzenia = new DateTime(2015, 11, 20),
                    Haslo = "b",
                    NazwaKonta = "JanuszRudnik"
                };
            }
        }
    
}
