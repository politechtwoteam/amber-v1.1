﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMBER_v1._1.Implementations;
using AMBER_v1._1.Interfaces;
using Moq;
using NUnit.Framework;

namespace AMBER_v1._1.Tests.Przelew
{
    class PrzelewTests : TestModels
    {
        private Mock<IHistoria> historiaMock;
        private IKonto iKonto;
        private Mock<IKredyt> iKredyt;
        

        [SetUp]
        public void SetUp()
        {
            var listaKont = new List<KontoModel>();
            listaKont.Add(KontoTestModelforPrzelew());
            listaKont.Add(KontoTestModel2forPrzelew());


            historiaMock = new Mock<IHistoria>();

            historiaMock.Setup(h => h.SetHistoriaWpis(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<float>()
                , It.IsAny<DateTime>(), It.IsAny<int?>()));
            
            iKredyt = new Mock<IKredyt>();

            iKonto = new Implementations.Konto(historiaMock.Object, iKredyt.Object);
            iKonto.TestowaListaKont(listaKont);
        }

        [TestCase(1100f, 21050f, 50f)]
        [TestCase(1055f, 100f, 100f)]
        [TestCase(100f, 0f, 50f)]
        public void PrzelewTest(float expectedFirst, float expectedSecond, float value)
        {
            iKonto.GetKontoById(1).Saldo = expectedFirst + value;
            iKonto.GetKontoById(2).Saldo = expectedSecond - value;
            var przelew = new Implementations.Przelew.Przelew(iKonto, historiaMock.Object, value);
            przelew.Wykonaj(1, 2, DateTime.Now);

            Assert.AreEqual(expectedFirst,iKonto.GetKontoById(1).Saldo);
            Assert.AreEqual(expectedSecond, iKonto.GetKontoById(2).Saldo);
        }

        [TestCase(1100f, 21050f, 50f)]
        public void CofnijPrzelewTest(float expectedFirst, float expectedSecond, float value)
        {
            iKonto.GetKontoById(1).Saldo = expectedFirst - value;
            iKonto.GetKontoById(2).Saldo = expectedSecond + value;
            var historia = HistoriaModel;
            historia.KontoFirst = iKonto.GetKontoById(1);
            historia.KontoSecond = iKonto.GetKontoById(2);

            historiaMock.Setup(h => h.GetHistoriaById(1)).Returns(historia);

            var przelew = new Implementations.Przelew.Przelew(iKonto, historiaMock.Object, value);
            przelew.Cofnij(1);

            Assert.AreEqual(expectedFirst, iKonto.GetKontoById(1).Saldo);
        }
    }

    public class TestModels
    {
        public static HistoriaModel HistoriaModel = new HistoriaModel
        {
            HistoriaId = 1,
            KontoSecond = null,
            KontoFirst = null,
            TypOperacji = "",
            DataOperaji = new DateTime(2015, 11, 20),
            Komunikat = "",
            Kwota = 0
        };

        public static KontoModel KontoTestModelforPrzelew()
        {
            return new KontoModel()
            {
                IdKonta = 1,
                Saldo = 2140.00f,
                HistoriaList = new List<HistoriaModel>(),
                KredytList = new List<KredytModel>(),
                DataUtworzenia = new DateTime(2015, 11, 20),
                Haslo = "a",
                NazwaKonta = "JanuszKowalski"
            };
        }

        public static KontoModel KontoTestModel2forPrzelew()
        {
            return new KontoModel()
            {
                IdKonta = 2,
                Saldo = 2540.00f,
                HistoriaList = new List<HistoriaModel>(),
                KredytList = null,
                DataUtworzenia = new DateTime(2015, 11, 20),
                Haslo = "b",
                NazwaKonta = "JanuszRudnik"
            };
        }
    }
}
